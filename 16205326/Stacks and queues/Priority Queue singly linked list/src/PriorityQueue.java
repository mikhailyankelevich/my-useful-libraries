import java.util.LinkedList;

public class PriorityQueue<E> implements PriorityQueueInterface<E> {
	private LinkedList<E> list ;

	//constructor method 
	public PriorityQueue() {
		list = new LinkedList<E>();
	}

	/**
	 * Returns (but does not remove) an entry with minimal key.
	 * 
	 * @return entry having a minimal key (or null if empty)
	 */
	public E min() {
		if (list.isEmpty())
			return null;
		return list.getFirst();
	}

	/**
	 * Removes and returns an entry with minimal key.
	 * 
	 * @return the removed entry (or null if empty)
	 */
	public E removeMin() {
		if (list.isEmpty())
			return null;
		return list.removeFirst();
	}
	//returns size of the queue
	public int size() {
		return list.size();
	}
	
	//this method  calls insert method and inserts a new value ( 2 variation of the same fucntion )
	public void add(E input) {
		insert(input);
	}

	@Override
	//tells if queue is empty
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return list.isEmpty();
	}

	@SuppressWarnings("unchecked")
	@Override
	//this function insetts the value
	public void insert(E value) {
		// TODO Auto-generated method stub
		if (list.isEmpty())//if list is empty,then inserts in the beginning if not  compares values until all larger keys are used and inncerts in there
			list.addFirst(value);
		else {
			Object tmp = list.get(0);
			boolean added=false;//value which indicates if value was already incerted( not to incert it  again in  the end
			Comparable<E> key = (Comparable<E>) value;
			for (int i = 0; i < list.size(); i++) {
				tmp = list.get(i);
				if (((Comparable<? super E>) value).compareTo((E) tmp) >= 0) 
				{
					list.add(i, value);
					added=true;
					break;
				}

				
			}
			if(!added)//checking if value already added
			list.add(value);
		}
	}

	public String toString() {
		return list.toString();
	}

	public static void main(String[] crs) {
		PriorityQueue<Integer> pq = new PriorityQueue<Integer>();

		pq.add(10);
		System.out.println(pq);
		pq.add(11);
		System.out.println(pq);
		pq.add(12);
		System.out.println(pq);
		pq.add(13);
		System.out.println(pq);
		pq.add(11);
		System.out.println(pq);
		pq.add(11);
		System.out.println(pq);
		pq.add(12);
		System.out.println(pq);
		pq.add(13);
		System.out.println(pq);
		pq.add(11);
		System.out.println(pq);
	}

}
