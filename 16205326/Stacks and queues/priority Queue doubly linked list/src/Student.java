

public class Student implements Comparable<Student>{
	private String name;
	private int age;
	private Double GPA;
	
	public Student() {
	}
	public Student(String name,int age, Double GPA) {
		this.name=name;
		this.age=age;
		this.GPA=GPA;
	}
	
	@Override
	//compare to methods, so students could be compared by their gpa
	public int compareTo(Student o) {
		// TODO Auto-generated method stub
		
		return Double.compare(this.GPA,o.GPA);
	}
	
	//method returns age of the student
	public int getAge() {
		return this.age;
	}
	
	//method returns name of the student
	public String getName() {
		return this.name;
	}
	
	//method returns GPA of the student
	public Double getGPA() {
		return this.GPA; 
	}
	
	//method sets age of the student
	public void setAge(int age) {
		this.age=age;
	}
	//method sets name of the student
	public void setName(String name) {
		this.name=name;
	}
	//method sets gpa of the student
	public void setGPA(Double GPA) { 
		this.GPA=GPA;
	}
	
	public String toString() {
		
		return name+",\t"+age+",\t"+GPA.toString();
	}
	
	public static void main(String[] args){
		// Creating Priority queue
		PriorityQueue<Student> pq = new PriorityQueue<Student>();
	
		// Invoking a parameterised Student constructor with
		Student student1 = new Student("Nataly Ware", 21, 4.0);
		Student student2 = new Student("Mira Weiss", 19, 3.5);
		Student student3 = new Student("Emilie Gibbs", 20, 3.2);
		Student student4 = new Student("Lisa Boone", 22, 4.7);
		Student student5 = new Student("Karsyn Terry", 20, 4.8);
		Student student6 = new Student("Jeremy Schwartz", 18, 4.6);
		Student student7 = new Student("Aleah Gaines", 19, 4.1);
		Student student8 = new Student("Arianna Reeves", 20, 3.9);
		Student student9 = new Student("Walker Holloway", 22, 3.8);
		Student student10 = new Student("Adelyn Walter", 24, 4.95);
		Student student11 = new Student("Damion Sanders", 25, 3.2);
		Student student12 = new Student("Aimee Quinn", 21, 2.7);
		
		pq.add(student1);
		pq.add(student2);
		pq.add(student3);
		pq.add(student4);
		pq.add(student5);
		pq.add(student6);
		pq.add(student7);
		pq.add(student8);
		pq.add(student9);
		pq.add(student10);
		pq.add(student11);
		pq.add(student12);
		 //...
		
		 // Printing names of students in priority order 
		 while (!pq.isEmpty()) {
		 System.out.println(pq.removeMin());
		 }
	 }
}