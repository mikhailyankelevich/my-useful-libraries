
public  class linkedListStack<E>  implements Stack<E>  {

	
	private SinglyLinkedList<E> list =new SinglyLinkedList<E>();
	
	@Override
	public int size() {
		
		return list.size();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return list.isEmpty();
	}
	@Override
	public void push(E input) {
		// TODO Auto-generated method stub
		list.addFirst(input);
	}

	@Override
	public E top() {
		// TODO Auto-generated method stub
		return list.first();
	}

	@Override
	public E pop() {
		// TODO Auto-generated method stub
		return list.removeFirst();
	}

	public String toString() {
		
		return list.toString();
	}
	
	public static void main (String[]crs)
	{
		linkedListStack<Integer> f=new linkedListStack<Integer>();
		f.push(10);
		f.push(11);
		f.push(12);
		f.push(13);
		f.push(14);
		f.push(15);
		f.push(16);
		f.push(17);
		f.push(18);
		f.push(19);
		f.push(20);
		f.push(21);
		f.push(22);
		f.push(23);
		f.pop();
		f.pop();
//		
		System.out.println(f.toString());
		System.out.println(f.top());
		
		f.pop();
		f.pop();
		System.out.println(f.toString());
		System.out.println(f.top());
		
	}
	
	
}