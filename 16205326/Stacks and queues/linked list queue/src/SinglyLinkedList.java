/*
This is the generic version of the SinglyLinkedList

You need to change the signature from:

public class SinglyLinkedList {

to:

public class SinglyLinkedList<E> {

and update all the other cases where 'String' should be replaced with 'E'.

 */

public class SinglyLinkedList<E> {

    static class Node<E> {
        private E data;
        private Node <E> next;
        
        public Node(E e, Node<E> n) {
            this.data = e;
            this.next = n;
        }
        public E getData() {
            // TODO
        	return this.data;
        }
        public Node<E> getNextNode() {
            // TODO
        	return this.next;
        }
        public void setNextNode(Node<E> next)
        {
        	this.next=next;
        }
    }

    private Node<E> head = null;
    private int size=0;
    public SinglyLinkedList() {   //constructor
    }

    public int size() {	
    	
    	return size;
    }
    
    
    public boolean isEmpty() {
        // TODO
    	if (head==null)
    	{
    		return true;
    	}
    	else 
    	return false;
    }

    public E first() {
        // TODO
    	return this.head.getData();
    }

    public E last() {
        // TODO
    	Node<E> temp = this.head;
    	E toReturn = null;
    	while(temp.getNextNode()!=null)//crolling till the end 
    	{
    		toReturn=temp.getData();
    		temp=temp.getNextNode();
    	}
    	return toReturn;
    }

    public void addFirst(E data) {
        // TODO
    	
    	Node<E> temp=new Node<E>(data, this.head);
    	this.head=temp;
    	size++;
    	
    }
    public E get(int i) {//this method gets value at position i
		   Node<E> toReturn=this.head;
		   if (isEmpty())
			   toReturn=toReturn.getNextNode();
		   else
		   for(int j=0; j<i; j++)//crolling to the ith node 
		   {
			   toReturn=toReturn.getNextNode();
		   }
		   return toReturn.getData();
	}
    
    public Node<E> getNode(int i) {//this method gets node at position i
    	Node<E> toReturn=this.head;
    	if (isEmpty())
			   toReturn=toReturn.getNextNode();
		   else
		   for(int j=0; j<i; j++)//crolling to the ith node 
		   {
			   toReturn=toReturn.getNextNode();
		   }
		   return toReturn;
    }
    
    public void add(E data, int i) {//this method adds node at position i
    	Node<E> next=getNode(i);
    	Node<E> tmp=new Node(data, next);
    	getNode(i-1).setNextNode(tmp);
    	size++;
    	
    }
    
    public E remove(int i) {//this method removes node at position i
    	Node<E> tmp= getNode(i-1);
    	E toReturn=tmp.getNextNode().getData();
    	tmp.setNextNode(getNode(i+1));
    	size--;
    	return toReturn;
    	
    }
	   
	   

    public void addLast(E data) {
	        // TODO
	    if(this.head!=null) {
	    	Node<E> currentTail=this.head;
	    	while(currentTail.getNextNode()!=null) {//crolling to the end and adding new node
				if(currentTail.getNextNode()!=null) {
					currentTail=currentTail.getNextNode();
				}
				else break;
	    	}
	    	
	    	Node<E> tmp=new Node<E>(data, null);
	    	currentTail.setNextNode(tmp);
    	}else if (this.head==null)
    	{
    		this.head=new Node<E>(data,null);
    	}
	    size++;
	    	
    	
    }

    public E removeFirst() {
        // TODO
    	Node<E> toReturn=this.head;
    	this.head=head.getNextNode();
    	size--;
    	return toReturn.getData();
    	
    }

    public E removeLast() {
        // TODO
    	Node<E> currentTail=this.head;
    	Node<E> tmp=this.head;
    	while(currentTail!=null) {//crolling &removing
			if(currentTail.getNextNode()!=null) {
				tmp=currentTail;
				currentTail=currentTail.getNextNode();
			}
			else break;
    	}
    	
    	tmp.setNextNode(null);
    	size--;
    	return currentTail.getData();
    }

    public String toString() {
    	String output = new String();
    	Node<E> tmp=this.head;
		output += "size=" + size() + "\n";
		while(tmp!=null) {
			output += tmp.getData() + "\n";
			if(tmp.getNextNode()!=null)
			tmp=tmp.getNextNode();
			else break;
		}
		return output;
    }
    
    public static void main(String [] args) {
        SinglyLinkedList<String> ll = new SinglyLinkedList<String>();
        ll.addFirst("Java");
        ll.addFirst("Swift");
        ll.addFirst("remove this");
        ll.addLast("c");
        ll.addLast("c++");
        ll.addLast("c#");
        ll.addLast("python");
        ll.addLast("delete this");
        ll.removeFirst();
        ll.removeLast();
        System.out.println(ll.toString());
        ll.add("test1", 1);
        System.out.println(ll.toString());
        System.out.println(ll.get(0));
        System.out.println(ll.getNode(100));
        ll.remove(3);
        

        System.out.println(ll.toString());
    }
}