public class linkedListQueue<E>{


	private SinglyLinkedList<E> list =new SinglyLinkedList<E>();//initializing singly list array
	
	public void enqueue(E input) {
		list.addLast(input);
	}
	
	public E dequeue() {
		if(empty()) {
			return null;
		}
			E toReturn=list.first();
			list.removeFirst();
			return toReturn;

	}
	
	public int size() {
		return list.size();
	}
	
	public boolean empty() {
		return list.isEmpty();
	}
	
	public E front() {
		return list.first();
	}
	
	public String toString() {
		return list.toString();
	}
	
	public static void main(String [] crs) {
		linkedListQueue<Integer> func=new linkedListQueue<Integer>();
		func.enqueue(10);
		func.enqueue(9);
		func.enqueue(8);
//		func.dequeue();
		func.enqueue(7);
		func.enqueue(6);
		func.enqueue(5);
		func.enqueue(4);
		func.dequeue();
		func.enqueue(3);
		func.enqueue(2);
		func.enqueue(1);

		
		
		
		System.out.println(func);
	}

}