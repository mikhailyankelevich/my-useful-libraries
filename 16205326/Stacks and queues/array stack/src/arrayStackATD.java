public class arrayStackATD<E> implements Stack<E>{
	public static void main (String[] src) {
		arrayStackATD<Integer> f=new arrayStackATD<Integer>();
		f.push(10);
		f.push(11);
		f.push(12);
		f.push(13);
		f.push(14);
		f.push(15);
		f.push(16);
		f.push(17);
		f.push(18);
		f.push(19);
		f.push(20);
		f.push(21);
		f.push(22);
		f.push(23);
		f.pop();
		f.pop();
//		
		System.out.println(f.toString());
		System.out.println(f.top());
		
		f.pop();
		f.pop();
		System.out.println(f.toString());
		System.out.println(f.top());
	}
	
	
	
	private static final int CAPACITY=1000;
	private int size=0;
	private E [] arr;
	
	
	public arrayStackATD(int capacity)
	{
		arr=(E[]) new Object[capacity];
	}
	public arrayStackATD()
	{
		arr=(E[]) new Object[CAPACITY];
	}
	
	
	
	@Override	
	public int size() {
		return this.size;
	}
	
	@Override
	public boolean isEmpty()
	{
		if (size==0)
			return true;
		return false;
	}
	
	@Override
	public void push(E input) {//add function
		this.arr[size()]=input;//inserting top
		size++;
	}
	
	
	
	@Override
	public E pop() {//pop function
		if(size==0)
			return null;
		else {
			E toReturn=this.arr[size()-1];//taking top
			this.arr[size()-1]=null;
			size--;
			return toReturn;
		}	
	}
	
	@Override
	public E top() {
		if(size==0)
			return null;
			return this.arr[size()-1];
	}
	
	
	public String toString() {
		String toReturn=" ";
		for(int i=size()-1; i>=0;i--)
		{
			toReturn+=this.arr[i]+" ";
		}
		return toReturn;
	}
	
}