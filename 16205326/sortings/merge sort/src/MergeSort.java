import java.util.Arrays;

public class MergeSort{
    	
	public static void main (String [] src)
	{
		int[] array=  {1,2,3,4,7,8,9,1,10,21,19,183,131,12};
        MergeSort run= new MergeSort();
        
		System.out.println(array.length);
		run.sortDecrese(array,0,array.length-1);
		
		System.out.println("Decrese: "+Arrays.toString(array));
		
		run.sortIncrese(array,0,array.length-1);
		System.out.println("increse: " +Arrays.toString(array));
		
		char[] arrayChar= {'a','f','e','t','s','f','f','w','q','e','h','d','x','o'};
		System.out.println(arrayChar.length);
		run.sortDecrese(arrayChar,0,arrayChar.length-1);
		
		System.out.println("Decrese: "+Arrays.toString(arrayChar));
		
		run.sortIncrese(arrayChar,0,arrayChar.length-1);
		System.out.println("increse: " +Arrays.toString(arrayChar));
	}
    	
    	
    	
    	
    	
    	
    	//sorting of int
    	
    	
    	
    	private void mergeIncrese(int arr[], int beginning, int middle, int end) 
    { 
        // Find sizes of two subarrays to be merged 
        int n1 = middle - beginning + 1; 
        int n2 = end - middle; 
  
    
        int L[] = new int[n1];
        int R[] = new int[n2];
  
        
        for (int i=0; i<n1; ++i) //filling tmp array 
            L[i] = arr[beginning + i]; 
        for (int j=0; j<n2; ++j) 
            R[j] = arr[middle + 1+ j]; 
  
        // Indexes of first and second tmp arrays
        int i = 0, j = 0;
        int tmp = beginning; //index of a merged array
        while (i < n1 && j < n2) 
        { 
            if (L[i]<=R[j]) 
            { 
                arr[tmp]=L[i]; 
                i++; 
            } 
            else
            { 
                arr[tmp]=R[j]; 
                j++; 
            } 
            tmp++; 
        } 
  //Copying the untouched parts( will work with only 1 array)
       for(int itmp=i ;itmp < n1;itmp++) 
        { 
            arr[tmp]=L[itmp]; 
            tmp++; 
        } 
        for(int jtmp=j; jtmp<n2;jtmp++) 
        { 
            arr[tmp] =R[jtmp]; 
            tmp++; 
        } 
    } 
  
    
    	
    //this function is recursive 
    void sortIncrese(int arr[], int beginningPos, int endPos) 
    { 
        if (beginningPos < endPos) 
        { 
            // Finding the middl
            int middle = (beginningPos+endPos)/2; 
            // Dividing into 2 halves and sort
            sortIncrese(arr, beginningPos, middle); 
            sortIncrese(arr , middle+1, endPos); 
            // Merge the sorted halves 
            mergeIncrese(arr, beginningPos, middle, endPos); 
        } 
    } 
    
    
    
    
    
    
    //sorting of double 
    
    
    
    
   	private void mergeIncrese(double arr[], int beginning, int middle, int end) 
    { 
        // Find sizes of two subarrays to be merged 
        int n1 = middle - beginning + 1; 
        int n2 = end - middle; 
  
    
        double L[] = new double[n1];
        double R[] = new double[n2];
  
        for (int i=0; i<n1; ++i) //filling tmp array 
            L[i] = arr[beginning + i]; 
        for (int j=0; j<n2; ++j) 
            R[j] = arr[middle + 1+ j]; 
  
        // Indexes of first and second tmp arrays
        int i = 0, j = 0;
        int tmp = beginning; //index of a merged array
        while (i < n1 && j < n2) 
        { 
            if (L[i]<=R[j]) 
            { 
                arr[tmp]=L[i]; 
                i++; 
            } 
            else
            { 
                arr[tmp]=R[j]; 
                j++; 
            } 
            tmp++; 
        } 
  //Copying the untouched parts( will work with only 1 array)
       for(int itmp=i ;itmp < n1;itmp++) 
        { 
            arr[tmp]=L[itmp]; 
            tmp++; 
        } 
        for(int jtmp=j; jtmp<n2;jtmp++) 
        { 
            arr[tmp] =R[jtmp]; 
            tmp++; 
        } 
    } 
  
    
    	
    //this function is recursive 
    void sortIncrese(double arr[], int beginningPos, int endPos) 
    { 
        if (beginningPos < endPos) 
        { 
            // Finding the middl
            int middle = (beginningPos+endPos)/2; 
  
            // Dividing into 2 halves and sort
            sortIncrese(arr, beginningPos, middle); 
            sortIncrese(arr , middle+1, endPos); 
  
            // Merge the sorted halves 
            mergeIncrese(arr, beginningPos, middle, endPos); 
        } 
    } 
    
    
    
    
    
    
    
    //sorting for char
    
    
    
    
    
  	private void mergeIncrese(char arr[], int beginning, int middle, int end) 
    { 
        // Find sizes of two subarrays to be merged 
        int n1 = middle - beginning + 1; 
        int n2 = end - middle; 
  
        
        char L[] = new char[n1];
        char R[] = new char[n2];
  
        
        for (int i=0; i<n1; ++i) //filling tmp array 
            L[i] = arr[beginning + i]; 
        for (int j=0; j<n2; ++j) 
            R[j] = arr[middle + 1+ j]; 
  
        // Indexes of first and second tmp arrays
        int i = 0, j = 0;
        int tmp = beginning; //index of a merged array
        while (i < n1 && j < n2) 
        { 
            if (L[i]<=R[j]) 
            { 
                arr[tmp]=L[i]; 
                i++; 
            } 
            else
            { 
                arr[tmp]=R[j]; 
                j++; 
            } 
            tmp++; 
        } 
  //Copying the untouched parts( will work with only 1 array)
       for(int itmp=i ;itmp < n1;itmp++) 
        { 
            arr[tmp]=L[itmp]; 
            tmp++; 
        } 
        for(int jtmp=j; jtmp<n2;jtmp++) 
        { 
            arr[tmp] =R[jtmp]; 
            tmp++; 
        } 
    } 
  
    
    	
    //this function is recursive 
    void sortIncrese(char arr[], int beginningPos, int endPos) 
    { 
        if (beginningPos < endPos) 
        { 
            // Finding the middl
            int middle = (beginningPos+endPos)/2; 
  
            // Dividing into 2 halves and sort
            sortIncrese(arr, beginningPos, middle); 
            sortIncrese(arr , middle+1, endPos); 
  
            // Merge the sorted halves 
            mergeIncrese(arr, beginningPos, middle, endPos); 
        } 
    } 
    
    
    
    
    
    
    // decrese merge functions (as above )!!!!!!!!!!!!!!!!!!
//    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
	//sorting of int
	
	
	
	private void mergeDecrese(int arr[], int beginning, int middle, int end) 
{ 
    // Find sizes of two subarrays to be merged 
    int n1 = middle - beginning + 1; 
    int n2 = end - middle; 

    
    int L[] = new int[n1];
    int R[] = new int[n2];

    
    for (int i=0; i<n1; ++i) //filling tmp array 
        L[i] = arr[beginning + i]; 
    for (int j=0; j<n2; ++j) 
        R[j] = arr[middle + 1+ j]; 

    // Indexes of first and second tmp arrays
    int i = 0, j = 0;
    int tmp = beginning; //index of a merged array
    while (i < n1 && j < n2) 
    { 
        if (L[i]>=R[j]) 
        { 
            arr[tmp]=L[i]; 
            i++; 
        } 
        else
        { 
            arr[tmp]=R[j]; 
            j++; 
        } 
        tmp++; 
    } 
//Copying the untouched parts( will work with only 1 array)
   for(int itmp=i ;itmp < n1;itmp++) 
    { 
        arr[tmp]=L[itmp]; 
        tmp++; 
    } 
    for(int jtmp=j; jtmp<n2;jtmp++) 
    { 
        arr[tmp] =R[jtmp]; 
        tmp++; 
    } 
} 


	
//this function is recursive 
void sortDecrese(int arr[], int beginningPos, int endPos) 
{ 
    if (beginningPos < endPos) 
    { 
        // Finding the middl
        int middle = (beginningPos+endPos)/2; 
        // Dividing into 2 halves and sort
        sortDecrese(arr, beginningPos, middle); 
        sortDecrese(arr , middle+1, endPos); 
        // Merge the sorted halves 
        mergeDecrese(arr, beginningPos, middle, endPos); 
    } 
} 






//sorting of double 




	private void mergeDecrese(double arr[], int beginning, int middle, int end) 
{ 
    // Find sizes of two subarrays to be merged 
    int n1 = middle - beginning + 1; 
    int n2 = end - middle; 

    
    double L[] = new double[n1];
    double R[] = new double[n2];

    
    for (int i=0; i<n1; ++i) //filling tmp array 
        L[i] = arr[beginning + i]; 
    for (int j=0; j<n2; ++j) 
        R[j] = arr[middle + 1+ j]; 

    // Indexes of first and second tmp arrays
    int i = 0, j = 0;
    int tmp = beginning; //index of a merged array
    while (i < n1 && j < n2) 
    { 
        if (L[i]>=R[j]) 
        { 
            arr[tmp]=L[i]; 
            i++; 
        } 
        else
        { 
            arr[tmp]=R[j]; 
            j++; 
        } 
        tmp++; 
    } 
//Copying the untouched parts( will work with only 1 array)
   for(int itmp=i ;itmp < n1;itmp++) 
    { 
        arr[tmp]=L[itmp]; 
        tmp++; 
    } 
    for(int jtmp=j; jtmp<n2;jtmp++) 
    { 
        arr[tmp] =R[jtmp]; 
        tmp++; 
    } 
} 


	
//this function is recursive 
void sortDecrese(double arr[], int beginningPos, int endPos) 
{ 
    if (beginningPos < endPos) 
    { 
        // Finding the middl
        int middle = (beginningPos+endPos)/2; 

        // Dividing into 2 halves and sort
        sortDecrese(arr, beginningPos, middle); 
        sortDecrese(arr , middle+1, endPos); 

        // Merge the sorted halves 
        mergeDecrese(arr, beginningPos, middle, endPos); 
    } 
} 







//sorting for char





	private void mergeDecrese(char arr[], int beginning, int middle, int end) 
{ 
    // Find sizes of two subarrays to be merged 
    int n1 = middle - beginning + 1; 
    int n2 = end - middle; 

   
    char L[] = new char[n1];
    char R[] = new char[n2];

   
    for (int i=0; i<n1; ++i) //filling tmp array 
        L[i] = arr[beginning + i]; 
    for (int j=0; j<n2; ++j) 
        R[j] = arr[middle + 1+ j]; 

    // Indexes of first and second tmp arrays
    int i = 0, j = 0;
    int tmp = beginning; //index of a merged array
    while (i < n1 && j < n2) 
    { 
        if (L[i]>=R[j]) 
        { 
            arr[tmp]=L[i]; 
            i++; 
        } 
        else
        { 
            arr[tmp]=R[j]; 
            j++; 
        } 
        tmp++; 
    } 
//Copying the untouched parts( will work with only 1 array)
   for(int itmp=i ;itmp < n1;itmp++) 
    { 
        arr[tmp]=L[itmp]; 
        tmp++; 
    } 
    for(int jtmp=j; jtmp<n2;jtmp++) 
    { 
        arr[tmp] =R[jtmp]; 
        tmp++; 
    } 
} 


	
//this function is recursive 
void sortDecrese(char arr[], int beginningPos, int endPos) 
{ 
    if (beginningPos < endPos) 
    { 
        // Finding the middl
        int middle = (beginningPos+endPos)/2; 

        // Dividing into 2 halves and sort
        sortDecrese(arr, beginningPos, middle); 
        sortDecrese(arr , middle+1, endPos); 

        // Merge the sorted halves 
        mergeDecrese(arr, beginningPos, middle, endPos); 
    } 
} 
   }
    