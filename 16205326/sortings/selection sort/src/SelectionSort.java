import java.util.Arrays;

public  class SelectionSort{
	public static void main (String [] src)
	{
		int[] array=  {1,2,3,4,7,8,9,1,10,21,19,183,131,12};
		SelectionSort run= new SelectionSort();
		
		run.sortDecrese(array);
		
		System.out.println("Decrese: "+Arrays.toString(array));
		
		run.sortIncrese(array);
		System.out.println("increse: " +Arrays.toString(array));
		
		char[] arrayChar= {'a','f','e','t','s','f','f','w','q','e','h','d','x','o'};
		
		run.sortDecrese(arrayChar);
		
		System.out.println("Decrese: "+Arrays.toString(arrayChar));
		
		run.sortIncrese(arrayChar);
		System.out.println("increse: " +Arrays.toString(arrayChar));
	}
	
	
	private void swap(int[] array, int i, int j) {
		int tmp;
		tmp=array[i];
		array[i]=array[j];
		array[j]=tmp;
	}
	
	
   public int [] sortIncrese(int [] arr) 
    { 
        for (int i = 0; i < arr.length-1; i++) 
        { 
            // Find the minimum element in unsorted array 
            int min = i; 
            for (int j = i+1; j < arr.length; j++) 
                if (arr[j] < arr[min]) 
                    min = j; 
  
            swap(arr, min, i);//swap to the minimum
        } 
        return arr;
    } 
   
   public int [] sortDecrese(int [] arr) 
   { 
       for (int i = 0; i < arr.length-1; i++) 
       { 
           // Find the minimum element in unsorted array 
           int max = i; 
           for (int j = i+1; j < arr.length; j++) 
               if (arr[j] > arr[max]) 
                   max = j; 
 
           swap(arr, max, i);//swap to the minimum
       } 
       return arr;
   } 
    
    
    
   
   
    
   
   
   
   private void swap(double[] array, int i, int j) {//same for double
		double tmp;
		tmp=array[i];
		array[i]=array[j];
		array[j]=tmp;
	}
	
	
  public double[] sortIncrese(double [] arr) 
   { 
       
       for (int i = 0; i < arr.length-1; i++) 
       { 
           // Find the minimum element in unsorted array 
           int min = i; 
           for (int j = i+1; j < arr.length; j++) 
               if (arr[j] < arr[min]) 
                   min = j; 
 
           swap(arr, min, i);//swap to the minimum
       } 
       return arr;
   } 
  public double[] sortDecrese(double [] arr) 
  { 
      for (int i = 0; i < arr.length-1; i++) 
      { 
          // Find the minimum element in unsorted array 
          int max = i; 
          for (int j = i+1; j < arr.length; j++) 
              if (arr[j] > arr[max]) 
                  max = j;
          swap(arr, max, i);//swap to the minimum
      } 
      return arr;
  } 
    
  
  
  
  
  
  
  
  
	private void swap(char[] array, int i, int j) {//same for char
		char tmp;
		tmp=array[i];
		array[i]=array[j];
		array[j]=tmp;
	}
  
    
	public char[] sortIncrese(char[] arr) 
    { 
       
        for (int i = 0; i < arr.length-1; i++) 
        { 
            // Find the minimum element in array 
            int min = i; 
            for (int j = i+1; j < arr.length; j++) 
                if (arr[j] < arr[min]) 
                    min = j; 
  
          
            swap(arr, min, i);//swap to the minimum
        } 
        return arr;
    }
    public char[] sortDecrese(char[] arr) 
    { 
        for (int i = 0; i < arr.length-1; i++) 
        { 
            // Find the minimum element in unsorted array 
            int max = i; 
            for (int j = i+1; j < arr.length; j++) 
                if (arr[j] > arr[max]) 
                    max = j;
            swap(arr, max, i);//swap to the minimum
        } 
        return arr;
    } 
      
    
    
    
    
}