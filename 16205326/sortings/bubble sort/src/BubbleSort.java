import java.util.Arrays;

public class BubbleSort<E>{
	
	
	private void swap(Integer[] array, int i, int j) {
		Integer tmp;
		tmp=array[i];
		array[i]=array[j];
		array[j]=tmp;
	}
	
	public Integer[] sortIncrese(Integer[] array){
	
		for(int i=0;i<array.length; i++)
		{
		
			for(int j =i+1 ; j<array.length;j++)
			{
				if (array[i]>array[j])
				{
					swap(array,i, j);
				}
			}
		}
		return array;
	}
	
	
	public Integer[] sortDecrese(Integer[] array){

		for(int i=0;i<array.length; i++)
		{
			
			for(int j =i+1 ; j<array.length;j++)
			{
				if (array[i]<array[j])
				{
					swap(array,i, j);
				}
			}
		}
		return array;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	private void swap(Double[] array, int i, int j) {
		Double tmp;
		tmp=array[i];
		array[i]=array[j];
		array[j]=tmp;
	}
	
	public Double[] sortIncrese(Double[] array){
	
		for(int i=0;i<array.length; i++)
		{
		
			for(int j =i+1 ; j<array.length;j++)
			{
				if (array[i]>array[j])
				{
					swap(array,i, j);
				}
			}
		}
		return array;
	}
	
	
	public Double[] sortDecrese(Double[] array){
		
		for(int i=0;i<array.length; i++)
		{
			
			for(int j =i+1 ; j<array.length;j++)
			{
				if (array[i]<array[j])
				{
					swap(array,i, j);
				}
			}
		}
		return array;
	}
	
	
	
	
	
	
	
	
	
	private void swap(char[] array, int i, int j) {
		char tmp;
		tmp=array[i];
		array[i]=array[j];
		array[j]=tmp;
	}
	
	public char[] sortIncrese(char[] array){
	
		for(int i=0;i<array.length; i++)
		{
		
			for(int j =i+1 ; j<array.length;j++)
			{
				if (array[i]>array[j])
				{
					swap(array,i, j);
				}
			}
		}
		return array;
	}
	
	
	public char[] sortDecrese(char[] array){
		
		for(int i=0;i<array.length; i++)
		{
			
			for(int j =i+1 ; j<array.length;j++)
			{
				if (array[i]<array[j])
				{
					swap(array,i, j);
				}
			}
		}
		return array;
	}
	
	
	
	public static void main (String[] crs){
		Integer[] array=  {1,2,3,4,7,8,9,1,10,21,19,183,131,12};
		BubbleSort run= new BubbleSort();
		
		array=run.sortDecrese(array);
		
		System.out.println("Decrese: "+Arrays.toString(array));
		
		array=run.sortIncrese(array);
		System.out.println("increse: " +Arrays.toString(array));
		
		char[] arrayChar= {'a','f','e','t','s','f','f','w','q','e','h','d','x','o'};
		
		arrayChar=run.sortDecrese(arrayChar);
		
		System.out.println("Decrese: "+Arrays.toString(arrayChar));
		
		arrayChar=run.sortIncrese(arrayChar);
		System.out.println("increse: " +Arrays.toString(arrayChar));
		
	}
}