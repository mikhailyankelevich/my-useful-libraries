

import java.util.Iterator;



/**
 * A basic doubly linked list implementation.
 */
public class DoublyLinkedList<E> implements List<E> {

	private static class Node<E> {

		/** The element stored at this node */
		private E data; // reference to the element stored at this node

		/** A reference to the preceding node in the list */
		private Node<E> prev; // reference to the previous node in the list

		/** A reference to the subsequent node in the list */
		private Node<E> next; // reference to the subsequent node in the list

		/**
		 * Creates a node with the given element and next node.
		 *
		 * @param e the element to be stored
		 * @param p reference to a node that should precede the new node
		 * @param n reference to a node that should follow the new node
		 */
		public Node(E e, Node<E> p, Node<E> n) {
			data = e;
			prev = p;
			next = n;
		}

		// public accessor methods
		/**
		 * Returns the element stored at the node.
		 * 
		 * @return the element stored at the node
		 */
		public E getData() {
			return data;
		}
		
		public void setData(E data) {//this function sets data useful later
			this.data= data;
		}

		/**
		 * Returns the node that precedes this one (or null if no such node).
		 * 
		 * @return the preceding node
		 */
		public Node<E> getPrev() {
			return prev;
		}

		/**
		 * Returns the node that follows this one (or null if no such node).
		 * 
		 * @return the following node
		 */
		public Node<E> getNext() {
			return next;
		}

		// Update methods
		/**
		 * Sets the node's previous reference to point to Node n.
		 * 
		 * @param p the node that should precede this one
		 */
		public void setPrev(Node<E> p) {
			prev = p;
		}

		/**
		 * Sets the node's next reference to point to Node n.
		 * 
		 * @param n the node that should follow this one
		 */
		public void setNext(Node<E> n) {
			next = n;
		}
		
	} // ----------- end of nested Node class -----------

	// instance variables of the DoublyLinkedList
	/** Sentinel node at the beginning of the list */
	private Node<E> header; // header sentinel

	/** Sentinel node at the end of the list */
	private Node<E> trailer; // trailer sentinel

	/** Number of elements in the list (not including sentinels) */
	private int size = 0; // number of elements in the list

	/** Constructs a new empty list. */
	public DoublyLinkedList() {
		header = new Node<>(null, null, null); // create header
		trailer = new Node<>(null, header, null); // trailer is preceded by header
		header.setNext(trailer); // header is followed by trailer
	}

	// public accessor methods
	/**
	 * Returns the number of elements in the linked list.
	 * 
	 * @return number of elements in the linked list
	 */
	public int size() {
		return size;
	}

	/**
	 * Tests whether the linked list is empty.
	 * 
	 * @return true if the linked list is empty, false otherwise
	 */
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * Returns (but does not remove) the first element of the list.
	 * 
	 * @return element at the front of the list (or null if empty)
	 */
	public E first() {
		// TODO
		return this.header.getData();//return the data from the header
	}

	/**
	 * Returns (but does not remove) the last element of the list.
	 * 
	 * @return element at the end of the list (or null if empty)
	 */
	public E last() {
		// TODO
		return this.trailer.getData();//return the data form the trailer
	}

	// public update methods
	/**
	 * Adds an element to the front of the list.
	 * 
	 * @param e the new element to add
	 */
	public void addFirst(E e) {
		addBetween(e, header, header.getNext()); // place just after the header
		size ++;
	}

	/**
	 * Adds an element to the end of the list.
	 * 
	 * @param e the new element to add
	 */
	public void addLast(E e) {
		addBetween(e, trailer.getPrev(), trailer); // place just before the trailer
		size++;
	}

	/**
	 * Removes and returns the first element of the list.
	 * 
	 * @return the removed element (or null if empty)
	 */
	public E removeFirst() {
		// TODO
		
		if (!this.isEmpty())//if the list is not empty set header too the header.getNext()+ decrease the size
		{
			Node<E> tmp=this.header.getNext();
			E toReturn = this.header.getData();
			this.header=tmp;
			size--;
			return toReturn;
		}
		
		return null;
	}

	/**
	 * Removes and returns the last element of the list.
	 * 
	 * @return the removed element (or null if empty)
	 */
	public E removeLast() {//if the list is not empty set trailer.getPrev too trailer+ decrease the size
		// TODO
		if (!this.isEmpty())
		{
			Node<E> tmp=this.trailer.getPrev();
			E toReturn = this.trailer.getData();
			this.trailer=tmp;
			size--;
			return toReturn;
		}
		return null;
	}

	// private update methods
	/**
	 * Adds an element to the linked list in between the given nodes. The given
	 * predecessor and successor should be neighboring each other prior to the call.
	 *
	 * @param predecessor node just before the location where the new element is
	 *                    inserted
	 * @param successor   node just after the location where the new element is
	 *                    inserted
	 */
	private void addBetween(E e, Node<E> predecessor, Node<E> successor) {
		// TODO
		Node<E> tmp= new Node<E>(e,predecessor,successor);
		predecessor.setNext(tmp);
		successor.setPrev(tmp);
		
	}

	/**
	 * Removes the given node from the list and returns its element.
	 * 
	 * @param node the node to be removed (must not be a sentinel)
	 */
	private E remove(Node<E> node) {
		// TODO
		E toReturn=node.getData();
		if (!this.isEmpty() && node.getNext().getData()!=null && node.getPrev().getData()!=null) {//checking if it is in the  middle, beginning or the end
			node.getPrev().setNext(node.getNext());
		}
		else if (node.getNext().getData()!=null && node.getPrev().getData()==null)//if first
			this.removeFirst();
		else if (node.getNext().getData()==null && node.getPrev().getData()!=null)//if last
			this.removeLast();
		
		size--;
		return toReturn;
	}

	private Node<E> getNode(int position)//custom function, created as this action was needed multiple times ( saves typing
	{
		Node<E> tmp = header.getNext();
		for (int j = 0; j < position; j++) {
			tmp = tmp.next;
		}
		return tmp;
	}
	@Override
	public Iterator<E> iterator() {//What does it do
		// TODO 
		Node<E> tmp= header.getNext();//pointing  to the first not empty value 

		if(tmp==this.trailer)
		return null;
		
		return new ListIterator<E>();
		
	}
	
	private class ListIterator<T> implements Iterator<T> {//list itarator for the Iterator function

		
		Node<T> tmp = (Node<T>) header;

		
		public boolean hasNext() {
			return tmp != null;
		}
		
		public T next() {
			T toReturn = tmp.getData();
			tmp = tmp.getNext();
			return toReturn;
		}
	}
	@Override
	public E get(int i) {
		// TODO 
		
		return getNode(i).getData();
	}

	@Override
	public void set(int i, E e) {
		// TODO 
		getNode(i).setData(e);

	}

	@Override
	public void add(int i, E e) {
		// TODO
		Node<E> tmp=getNode(i);
		addBetween(e,tmp.getPrev(),tmp);
		size++;
	}

	@Override
	public E remove(int i) {
		// TODO Auto-generated method stub
		Node<E> tmp=getNode(i);
		E toReturn=tmp.getData();
		this.remove(i);//removes using the private function
		return toReturn;
	}

	/**
	 * Produces a string representation of the contents of the list. This exists for
	 * debugging purposes only.
	 */
	public String toString() {
		//TODO
		String output = new String();
    	Node<E> tmp=this.header.getNext();//using get next as first and last nodes are dummy nodes
		while(tmp.getNext()!=null) {
			output += tmp.getData() + "\n";
			if(tmp.getNext()!=null)
			tmp=tmp.getNext();
			else break;
		}
		return output;

	}

	public static void main(String[] args) {
		DoublyLinkedList<Integer> ll = new DoublyLinkedList<Integer>();
//		ll.add(0, -1);
		ll.addLast(10);
		ll.addLast(210);
		ll.addLast(30);
//		ll.remove(3);
		ll.addLast(500);
		System.out.println(ll);
	}
}
