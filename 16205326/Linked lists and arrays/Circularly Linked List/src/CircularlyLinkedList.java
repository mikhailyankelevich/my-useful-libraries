

public class CircularlyLinkedList<E> {

	private static class Node<E> {
		private E data;
		private Node<E> next;

		private Node(E e, Node<E> n) {
			this.data = e;
			this.next = n;
		}

		public E getData() {
			return this.data;
		}

		public void setData(E e) {
			this.data = e;
		}

		public Node<E> getNext() {
			return this.next;
		}

		public void setNext(Node<E> n) {
			this.next = n;
		}

		public String toString() {
			return this.data.toString();
		}
	}

	private int size = 0;
	private Node<E> tail = null;


	private Node<E> getNode(int index) {
		Node<E> curr = tail;
		for (int i = 0; i < index; ++i) {
			curr = curr.next;
		}
		return curr;
	}

	
	
	public void rotate() { // rotate the first element to the back of the list
		if (tail != null) // if empty, do nothing
			tail = tail.next; // the old head becomes the new tail
	}
	
	
	public int size() {//return the size
		return size;
	}

	public boolean isEmpty() {//tells if list is empty
		return size == 0;
	}

	
	
	public E first() {
		if (isEmpty())
			return null;
		return tail.getNext().getData();//returning the first element
	}

	public E last() { // returns the last element
		if (isEmpty())
			return null;
		return tail.data;
	}

	
	

	public void addFirst(E e) { // adds element to the front of the list
		if (size == 0) {
			tail = new Node<>(e, null);
			tail.setNext(tail); // link to itself
		} else {
			Node<E> n = new Node<>(e, tail.getNext());//creating a new node with link to the tail
			tail.setNext(n);//setting tail next to new node including it into the list
		}
		size++;
	}

	public void addLast(E e) { // adds element e to the end of the list
		addFirst(e); // insert new element at front of list
		tail = tail.getNext(); // now new element becomes the tail
	}
	
	
	

	public E remove(int i) {
		Node<E> prev = getNode(i - 1);//getting the previous node
		Node<E> curr = prev.getNext();
		E toReturn = curr.getData();
		prev.setNext(curr.getNext());//skipping the node
		size--;
		return toReturn;
	}
	
	public E removeFirst() { // removes and returns the first element
		if (isEmpty()) 
			return null;
		return remove(0);
	}

	public E removeLast() {
		if (isEmpty()) 
			return null;
			return remove(size - 1);
		
	}
	
	
	
	
	public String toString() {
		String output = new String();
		output += "size=" + size() + "\n";
		for (int i = 0; i < size(); i++) {
			output += getNode(i).data + "\n";
		}
		return output;
	}

    public static void main(String [] args) {
        CircularlyLinkedList<String> ll = new CircularlyLinkedList<String>();
        ll.addFirst("Java");
        ll.addFirst("Swift");
        ll.addFirst("remove this");
        ll.addLast("c");
        ll.addLast("c++");
        ll.addLast("c#");
        ll.addLast("python");
        ll.addLast("delete this");
        System.out.println(ll.toString());
        ll.removeFirst();
        System.out.println(ll.toString());
        ll.removeLast();

        System.out.println(ll.toString());
    }
}