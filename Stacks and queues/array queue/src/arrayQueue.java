

public class arrayQueue<E>{
	private E arr[];//creating an array
	private int rear=0;//rear id
	private int front=0;//front id
	
	private final int CAPASITY=100000;
	@SuppressWarnings("unchecked")
	public arrayQueue(){
		this.arr=(E[]) new Object[this.CAPASITY]; //constructor with default capasity
	}
	@SuppressWarnings("unchecked")
	public arrayQueue(int capasity){//constructor with custom capasity
		this.arr=(E[]) new Object[capasity+1];
	}
	
	
	public void enqueue(E input) {
		arr[rear]=input;
		rear=(rear+1)%arr.length;//moving rear
	}
	
	public E dequeue() {
		if(empty())
			return null;
		E toReturn=arr[front];//adding value
		front=(front+1)%arr.length;//mooving front
		return toReturn;
	}
	
	public int size() {
		return (arr.length -rear+front)%arr.length;
	}
	
	public boolean empty() {
		return front==rear;
	}
	
	public E front() {
		return arr[front];
	}
	
	public String toString() {
		String toReturn ="";
		
		for(int i=front;i<rear;i++)//scanning array
			toReturn+=arr[i]+"\n";
		return toReturn;
	}
	
	public static void main(String [] crs) {
		arrayQueue<Integer> func=new arrayQueue<Integer>(10);
		func.enqueue(10);
		func.enqueue(9);
		func.enqueue(8);
		func.dequeue();
		func.enqueue(100);
		func.enqueue(10);
		func.enqueue(9);
		func.enqueue(8);
		func.dequeue();
		func.enqueue(100);
		func.enqueue(10);
		func.enqueue(8);

		
		
		
		System.out.println(func);
	}
}