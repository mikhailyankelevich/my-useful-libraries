package comp20010_tutorial_1;

//package snippets;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * @version 1.0 September 2017
 * 
 * @author Aonghus Lawlor
 */
public class ScoreBoard {
	private int maxEntries;
	private int numEntries; // number of actual entries
	private GameEntry[] board; // array of game entries (name and scores)

	public ScoreBoard(int capacity) {
		// TODO
		this.maxEntries = capacity;
		this.numEntries=0;
		this.board = new GameEntry[this.maxEntries];

	}

	/** Attempts to add a new score to the collection (if it is high enough). */
	public void add(GameEntry e) {
		if (this.numEntries==0) {
			
			board[0]=e;
			this.numEntries++;
			
		}else if (this.numEntries<this.maxEntries)
		{
			for (int i =0 ; i<this.numEntries;i++)
			{
				if (board[i].getScore()<e.getScore())
				{
					for (int j=numEntries; j>i;j--) {
						board[j]=board[j-1];
					}
					board[i]=e;
					this.numEntries++;
					break;
				}
			}
		}else if (this.numEntries==this.maxEntries)
		{
			for (int i =0 ; i<this.numEntries;i++)
			{
				if (board[i].getScore()<e.getScore())
				{
					for (int j=numEntries-1; j>i;j--) {
						board[j]=board[j-1];
					}
					board[i]=e;
					break;
				}
			}
		}
	}

	/** Attempts to remove an existing score from the collection */
	public GameEntry remove(int idRemove) throws IndexOutOfBoundsException {
//	 TODO
		GameEntry e=board[idRemove];
		for (int i =0 ; i<this.numEntries;i++)
		{
			if (i==idRemove)
			{
				
				for (int j=i; j<this.numEntries;j++) {
					board[j]=board[j+1];
				}
//				board[i]=e;
				break;
			}
		}
		
		return e;
	}

	public String toString() {
		// TODO
		String returnString="";
		for (GameEntry b:board) {
	
			returnString+=b.toString()+"\n";

		}
			
		return returnString;

	}


	public static void main(String[] args) {
		// TODO
		// create a new scoreboard
		ScoreBoard s = new ScoreBoard(5);

		File file = new File("scores.txt");
		try {
			Scanner scanner = new Scanner(file);
			while (scanner.hasNext()) {
				String line = scanner.nextLine();
				Scanner lineReader = new Scanner(line).useDelimiter(",\\s?+"); // comma followed by any number of spaces
				// TODO
				// read the name and score

				s.add(new GameEntry(lineReader.next(), lineReader.nextInt()));

				// create the GameEntry object
				// add to scoreboard
				lineReader.close();
			}
			scanner.close();

			// TODO: print out the scoreboard
			System.out.println(s.toString());
		} catch (FileNotFoundException e) {
			// TODO:
			System.out.println("!!!!!!!!!no file!!!!!!!!!!");
		}
	}

}
