
import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;
import java.util.Scanner;


public class Database{
	private String connectionString="jdbc:mysql://localhost:3306/lab2";
	private String login="root";
	private String password="Vepop05121998";
	Connection c ;
	
	 public Database() throws SQLException {
		 c= DriverManager.getConnection(this.connectionString, login, password);		 //open the database
	 }
	 
	 
	 public void shutDown() throws SQLException{
		c = DriverManager.getConnection(connectionString+";shutdown=true", login, password);//shutting down database( may be useful for not MYSQL 
	 }
	 
	 public void insertData(int IDBOX, int DATEPUT, int ISITFREE) throws SQLException {
		 String sql= "INSERT INTO `test`.`testmemory` (`IdBox`, `date`, `free`) VALUES ("+IDBOX+"," +DATEPUT+","+ISITFREE+");";
		 c.createStatement().executeUpdate(sql);
//		 c.commit(); //no need as autocommit is on 
		
	 }
	 
	 
	 public String toString () {
		 
		 String toReturn=" ";
		 
			PreparedStatement pst;
			try {
				pst = c.prepareStatement("select * from `test`.`testmemory`");
			    pst.clearParameters();
		        ResultSet rs = pst.executeQuery();
		        while(rs.next()){
		        	toReturn+=rs.getString(1)+" "+rs.getString(2)+" "+rs.getString(3)+"\n";
		        }
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println("nothing saved:(");
				return null;
			}
	    
	        
		 
		 return toReturn;
		 }
	 
	 
	 
	 
	 public void close() throws SQLException { //don't forget to close the database, especially local ones
		 c.close();
	 }
	 
	 
	 public String readToString(String fileName) {
		 Scanner scan;
		try {
			scan = new Scanner(new File(fileName));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("No file");
			return null;
		}
			String string = scan.toString(); 
			return string;
	 }
	 public static void main(String [] src)
	 {
		 try {
			Database dat=new Database();
			Random rand=new Random();
			dat.insertData(rand.nextInt(), 10, 100);
			
			System.out.println(dat);
			dat.close();
				System.out.println("finish");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("shit");
			e.printStackTrace();
		}
		
	 }
	 
}